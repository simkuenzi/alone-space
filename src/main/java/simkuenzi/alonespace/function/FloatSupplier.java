package simkuenzi.alonespace.function;

import java.util.function.Supplier;

public interface FloatSupplier extends Supplier<Float> {

    @Override
    default Float get() {
        return getAsFloat();
    }

    float getAsFloat();
}
