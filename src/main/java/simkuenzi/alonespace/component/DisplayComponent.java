package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import simkuenzi.alonespace.display.Display;
import simkuenzi.alonespace.display.DisplayControl;
import simkuenzi.alonespace.function.FloatSupplier;

import java.util.function.BooleanSupplier;

public class DisplayComponent implements Component {
    private AssetManager assetManager;
    private Positioning positioning;
    private String label;
    private FloatSupplier value;
    private FloatSupplier maxValue;
    private FloatSupplier demandedValue;
    private Orientation orientation;
    private int row;
    private float x;
    private float width;
    private boolean mirrored;

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, FloatSupplier value, FloatSupplier maxValue, FloatSupplier demandedValue, Orientation orientation, int row, float x, float width, boolean mirrored) {
        this.assetManager = assetManager;
        this.positioning = positioning;
        this.label = label;
        this.value = value;
        this.maxValue = maxValue;
        this.demandedValue = demandedValue;
        this.orientation = orientation;
        this.row = row;
        this.x = x;
        this.width = width;
        this.mirrored = mirrored;
    }

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, FloatSupplier value, FloatSupplier maxValue) {
        this(assetManager, positioning, label, value, maxValue, 0);
    }

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, FloatSupplier value, FloatSupplier maxValue, int row) {
        this(assetManager, positioning, label, value, maxValue, null, Orientation.LEFT, row);
    }

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, FloatSupplier value, FloatSupplier maxValue, FloatSupplier demandedValue, int row) {
        this(assetManager, positioning, label, value, maxValue, demandedValue, Orientation.LEFT, row);
    }

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, FloatSupplier value, FloatSupplier maxValue, Orientation orientation, int row) {
        this(assetManager, positioning, label, value, maxValue, null, orientation, row, 0, 200, false);
    }

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, FloatSupplier value, FloatSupplier maxValue, FloatSupplier demandedValue, Orientation orientation, int row) {
        this(assetManager, positioning, label, value, maxValue, demandedValue, orientation, row, 0, 200, false);
    }

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, FloatSupplier value, FloatSupplier maxValue, int row, int x, int width, boolean mirrored) {
        this(assetManager, positioning, label, value, maxValue, null, Orientation.LEFT, row, x, width, mirrored);
    }

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, FloatSupplier value, FloatSupplier maxValue, FloatSupplier demandedValue, int row, int x, int width, boolean mirrored) {
        this(assetManager, positioning, label, value, maxValue, demandedValue, Orientation.LEFT, row, x, width, mirrored);
    }

    public DisplayComponent(AssetManager assetManager, Positioning positioning, String label, BooleanSupplier value, Orientation orientation, int row) {
        this(assetManager, positioning, label, () -> value.getAsBoolean() ? 1 : 0, () -> 1, orientation, row);
    }

    @Override
    public void apply(Node node) {
        Spatial display = new Display(label, assetManager, width);
        if (orientation == Orientation.LEFT) {
            display.setLocalTranslation(positioning.getLeftX(x), positioning.getBottomY(row), 0);
        } else {
            display.setLocalTranslation(positioning.getRightX(x, width), positioning.getBottomY(row), 0);
        }
        DisplayControl control = new DisplayControl(assetManager, value, maxValue, demandedValue);
        control.setMirror(mirrored);
        display.addControl(control);
        node.attachChild(display);
    }
}
