package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;

public class CrossHairComponent implements Component {
    private AssetManager assetManager;
    private Positioning positioning;

    public CrossHairComponent(AssetManager assetManager, Positioning positioning) {
        this.assetManager = assetManager;
        this.positioning = positioning;
    }

    @Override
    public void apply(Node node) {
        Node crossHair = new Node();
        crossHair.setLocalTranslation(positioning.getCenterX(50), positioning.getCenterY(50), 0);
        addLine(crossHair, new Vector3f(0, 25, 0), new Vector3f(50, 25, 0));
        addLine(crossHair, new Vector3f(25, 0, 0), new Vector3f(25, 50, 0));
        node.attachChild(crossHair);
    }

    private void addLine(Node node, Vector3f start, Vector3f end) {
        Line line = new Line(start, end);
        Material material = assetManager.loadMaterial("Common/Materials/WhiteColor.j3m");
        Geometry geometry = new Geometry("Crosshair Line", line);
        geometry.setMaterial(material);
        node.attachChild(geometry);
    }
}
