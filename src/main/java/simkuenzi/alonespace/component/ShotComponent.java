package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.MeshCollisionShape;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import simkuenzi.alonespace.vessel.ShotControl;

public class ShotComponent implements Component {

    private AssetManager assetManager;
    private BulletAppState bulletAppState;
    private int collisionGroup;

    public ShotComponent(AssetManager assetManager, BulletAppState bulletAppState, int collisionGroup) {
        this.assetManager = assetManager;
        this.bulletAppState = bulletAppState;
        this.collisionGroup = collisionGroup;
    }

    @Override
    public void apply(Node node) {
        Box mesh = new Box(new Vector3f(.1f, .1f, 0), new Vector3f(-.1f, -.1f, 20));
        Geometry geometry = new Geometry("", mesh);
        geometry.setMaterial(assetManager.loadMaterial("Common/Materials/WhiteColor.j3m"));
        node.attachChild(geometry);
        ShotControl control = new ShotControl(new MeshCollisionShape(mesh));
        control.addCollideWithGroup(collisionGroup);
        node.addControl(control);
        bulletAppState.getPhysicsSpace().add(control);
        bulletAppState.getPhysicsSpace().addCollisionListener(control);
    }
}
