package simkuenzi.alonespace.component;

import com.jme3.scene.Node;

public interface Component {
    int SHIP_COLLISION_GROUP = 2;

    void apply(Node node);
}
