package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.InputManager;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import simkuenzi.alonespace.pilot.PlayerPilotControl;

public class PlayerComponent extends FighterComponent {

    private InputManager inputManager;
    private CameraNode cameraNode;

    public PlayerComponent(AssetManager assetManager, InputManager inputManager, BulletAppState bulletAppState, CameraNode cameraNode, Node explosionsNode, Node shotsNode) {
        super(assetManager, bulletAppState, explosionsNode, shotsNode, Vector3f.ZERO, Quaternion.ZERO);
        this.inputManager = inputManager;
        this.cameraNode = cameraNode;
    }

    @Override
    public void apply(Node node) {
        node.attachChild(cameraNode);
        node.setCullHint(Spatial.CullHint.Always);
        super.apply(node);
    }

    @Override
    protected Spatial getModel() {
        return getAssetManager().loadModel("simkuenzi/alonespace/ship/ship_concept.blend");
    }

    @Override
    protected Control getPilot() {
        return new PlayerPilotControl(inputManager);
    }
}
