package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;

public class AsteroidBeltComponent implements Component {
    private AssetManager assetManager;
    private BulletAppState bulletAppState;
    private Node radarNode;

    public AsteroidBeltComponent(AssetManager assetManager, BulletAppState bulletAppState) {
        this.assetManager = assetManager;
        this.bulletAppState = bulletAppState;
    }

    public AsteroidBeltComponent(AssetManager assetManager, BulletAppState bulletAppState, Node radarNode) {
        this.assetManager = assetManager;
        this.bulletAppState = bulletAppState;
        this.radarNode = radarNode;
    }

    @Override
    public void apply(Node node) {
        for (int z = 0; z < 10; z++) {
            for (int i = 0; i < 100; i++) {
                Node objectNode = new Node("Asteroid");
                node.attachChild(objectNode);
                objectNode.setLocalTranslation(-50 + i * 60, FastMath.nextRandomInt(-100, 100), FastMath.nextRandomInt(200 + 600 * z, 800 + 600 * z));
                Spatial object = assetManager.loadModel("simkuenzi/alonespace/rocks/rock_d_01.obj");
                object.setLocalScale(20f);
                RigidBodyControl heavy = new RigidBodyControl(500);
                heavy.setCollisionGroup(SHIP_COLLISION_GROUP);
                heavy.addCollideWithGroup(SHIP_COLLISION_GROUP);
                Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                material.setColor("Color", new ColorRGBA(.8f, .4f, .1f, 0));
                Texture texture = assetManager.loadTexture("simkuenzi/alonespace/rocks/rock_d_01_normal.tga");
                material.setTexture("ColorMap", texture);
                object.setMaterial(material);
                objectNode.attachChild(object);
                objectNode.addControl(heavy);
                bulletAppState.getPhysicsSpace().add(objectNode);
            }
        }
    }
}
