package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import simkuenzi.alonespace.radar.RadarControl;
import simkuenzi.alonespace.radar.RadarOrientation;
import simkuenzi.alonespace.radar.RadarScreen;

public class RadarComponent implements Component {
    private AssetManager assetManager;
    private Positioning positioning;
    private Spatial observer;
    private Node scannedNode;
    private Orientation orientation;
    private RadarOrientation radarOrientation;

    public RadarComponent(AssetManager assetManager, Positioning positioning, Spatial observer, Node scannedNode, Orientation orientation, RadarOrientation radarOrientation) {
        this.assetManager = assetManager;
        this.positioning = positioning;
        this.observer = observer;
        this.scannedNode = scannedNode;
        this.orientation = orientation;
        this.radarOrientation = radarOrientation;
    }

    @Override
    public void apply(Node node) {
        Spatial screen = new RadarScreen(assetManager);
        BoundingBox boundingBox = (BoundingBox) screen.getWorldBound();
        float width = boundingBox.getXExtent() * 2;
        float height = boundingBox.getYExtent() * 2;
        screen.addControl(new RadarControl(observer, scannedNode, radarOrientation, assetManager));
        if (orientation == Orientation.LEFT) {
            screen.setLocalTranslation(positioning.getLeftX(), positioning.getTopY(height), 0);
        } else {
            screen.setLocalTranslation(positioning.getRightX(width), positioning.getTopY(height), 0);
        }
        node.attachChild(screen);
    }
}
