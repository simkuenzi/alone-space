package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import simkuenzi.alonespace.vessel.VesselControl;

public abstract class FighterComponent implements Component {

    private AssetManager assetManager;
    private BulletAppState bulletAppState;
    private Node explosionsNode;
    private Node shotsNode;
    private Vector3f location;
    private Quaternion rotation;

    public FighterComponent(AssetManager assetManager, BulletAppState bulletAppState, Node explosionsNode, Node shotsNode, Vector3f location, Quaternion rotation) {
        this.assetManager = assetManager;
        this.bulletAppState = bulletAppState;
        this.explosionsNode = explosionsNode;
        this.shotsNode = shotsNode;
        this.location = location;
        this.rotation = rotation;
    }

    protected AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void apply(Node node) {
        node.attachChild(getModel());
        VesselControl ship = new VesselControl(new ExplosionComponent(assetManager), explosionsNode);
        newGun(node, new Vector3f(-5, -3, 10));
        newGun(node, new Vector3f(5, -3, 10));
        newGun(node, new Vector3f(-5, 3, 10));
        newGun(node, new Vector3f(5, 3, 10));
        node.addControl(ship);
        node.addControl(getPilot());
        ship.setDamping(.8f, .8f);
        ship.setCollisionGroup(SHIP_COLLISION_GROUP);
        ship.addCollideWithGroup(SHIP_COLLISION_GROUP);
        ship.setPhysicsLocation(location);
        ship.setPhysicsRotation(rotation);
        bulletAppState.getPhysicsSpace().addAll(node);
        bulletAppState.getPhysicsSpace().addTickListener(ship);
    }

    private void newGun(Node shipNode, Vector3f location) {
        Node node = new Node("Gun");
        node.setLocalTranslation(location);
        GunComponent gun = new GunComponent(assetManager, bulletAppState, shotsNode, SHIP_COLLISION_GROUP);
        gun.apply(node);
        shipNode.attachChild(node);
    }

    protected abstract Control getPilot();

    protected abstract Spatial getModel();
}
