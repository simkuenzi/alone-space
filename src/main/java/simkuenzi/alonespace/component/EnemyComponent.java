package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import simkuenzi.alonespace.pilot.AiPilotControl;

public class EnemyComponent extends FighterComponent {

    private Node targets;

    public EnemyComponent(AssetManager assetManager, BulletAppState bulletAppState, Node explosionsNode, Node shotsNode, Node targets, Vector3f location, Quaternion rotation) {
        super(assetManager, bulletAppState, explosionsNode, shotsNode, location, rotation);
        this.targets = targets;
    }

    @Override
    protected Spatial getModel() {
        Spatial model = getAssetManager().loadModel("simkuenzi/alonespace/ship/rusty_ship.blend");
        model.rotate(0, FastMath.PI, 0);
        return model;
    }

    @Override
    protected Control getPilot() {
        return new AiPilotControl(targets);
    }
}
