package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.scene.Node;
import simkuenzi.alonespace.vessel.GunControl;

public class GunComponent implements Component {

    private AssetManager assetManager;
    private BulletAppState bulletAppState;
    private Node shotsNode;
    private int collisionGroup;

    public GunComponent(AssetManager assetManager, BulletAppState bulletAppState, Node shotsNode, int collisionGroup) {
        this.assetManager = assetManager;
        this.bulletAppState = bulletAppState;
        this.shotsNode = shotsNode;
        this.collisionGroup = collisionGroup;
    }

    @Override
    public void apply(Node node) {
        ShotComponent shot = new ShotComponent(assetManager, bulletAppState, collisionGroup);
        AudioNode audioNode = new AudioNode(assetManager, "simkuenzi/alonespace/sound/qubodup-crash-mono.ogg");
        audioNode.setPositional(true);
        audioNode.setRefDistance(.5f);
        node.attachChild(audioNode);
        GunControl control = new GunControl(shotsNode, shot, audioNode);
        node.addControl(control);
    }
}
