package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import simkuenzi.alonespace.radar.RadarOrientation;
import simkuenzi.alonespace.vessel.GunControl;
import simkuenzi.alonespace.vessel.VesselControl;

import java.text.MessageFormat;
import java.util.ResourceBundle;

public class HudComponent implements Component {
    private AssetManager assetManager;
    private Positioning positioning;
    private Spatial player;
    private Node scannedNode;
    private ResourceBundle bundle = ResourceBundle.getBundle("simkuenzi/alonespace/labels");

    public HudComponent(AssetManager assetManager, Positioning positioning, Spatial player, Node scannedNode) {
        this.assetManager = assetManager;
        this.positioning = positioning;
        this.player = player;
        this.scannedNode = scannedNode;
    }

    @Override
    public void apply(Node node) {
        VesselControl ship = player.getControl(VesselControl.class);

        if (ship != null) {
            DisplayComponent conditionDisplay = new DisplayComponent(assetManager, positioning, bundle.getString("hull"), ship::getCondition, () -> 20);
            conditionDisplay.apply(node);

            DisplayComponent throttleDisplay = new DisplayComponent(assetManager, positioning, bundle.getString("throttle"),
                    () -> ship.getEnergyDistribution().getThrottle().getActualEnergy(),
                    () -> 1,
                    () -> ship.getEnergyDistribution().getThrottle().getDemandedEnergy(),
                    1);
            throttleDisplay.apply(node);

            for (int i = 0; i < ship.getGuns().size(); i++) {
                int row = i / 2 + 2;
                int column = i % 2;
                GunControl gun = ship.getGuns().get(i);
                String label = MessageFormat.format(bundle.getString("gun"), i + 1);
                DisplayComponent gunDisplay = new DisplayComponent(assetManager, positioning, label, () -> 1 - gun.getHeat(), () -> 1, row, column * 105, 95, column == 1);
                gunDisplay.apply(node);
            }

            DisplayComponent stealthDisplay = new DisplayComponent(assetManager, positioning, bundle.getString("stealth"),
                    () -> ship.getEnergyDistribution().getStealth().isEnabled(), Orientation.RIGHT, 0);
            stealthDisplay.apply(node);

            DisplayComponent engineDisplay = new DisplayComponent(assetManager, positioning, bundle.getString("engine"),
                    () -> ship.getEnergyDistribution().getEngine().getActualEnergy(),
                    () -> 1,
                    Orientation.RIGHT, 1);
            engineDisplay.apply(node);

            DisplayComponent gunCoolerDisplay = new DisplayComponent(assetManager, positioning, bundle.getString("gunCooling"),
                    () -> ship.getEnergyDistribution().getGunCooling().getActualEnergy(),
                    () -> 1,
                    () -> ship.getEnergyDistribution().getGunCooling().getDemandedEnergy(),
                    Orientation.RIGHT, 2);
            gunCoolerDisplay.apply(node);
        }

        RadarComponent rearRadar = new RadarComponent(assetManager, positioning, player, scannedNode, Orientation.LEFT, RadarOrientation.REAR);
        rearRadar.apply(node);

        RadarComponent frontRadar = new RadarComponent(assetManager, positioning, player, scannedNode, Orientation.RIGHT, RadarOrientation.FRONT);
        frontRadar.apply(node);

        CrossHairComponent crossHair = new CrossHairComponent(assetManager, positioning);
        crossHair.apply(node);
    }
}
