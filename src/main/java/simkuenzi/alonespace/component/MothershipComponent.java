package simkuenzi.alonespace.component;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import simkuenzi.alonespace.radar.ReflectiveControl;

import java.io.IOException;

public class MothershipComponent implements Component {
    private AssetManager assetManager;
    private BulletAppState bulletAppState;

    public MothershipComponent(AssetManager assetManager, BulletAppState bulletAppState) {
        this.assetManager = assetManager;
        this.bulletAppState = bulletAppState;
    }

    @Override
    public void apply(Node node) {
        Spatial object = assetManager.loadModel("simkuenzi/alonespace/ship/ship_concept.blend");
        object.setLocalScale(40f);
        RigidBodyControl heavy = new RigidBodyControl(500);
        heavy.setCollisionGroup(SHIP_COLLISION_GROUP);
        heavy.addCollideWithGroup(SHIP_COLLISION_GROUP);
        node.attachChild(object);
        node.addControl(heavy);
        node.addControl(new ReflectiveControl() {
            @Override
            public float getReflection() {
                return 1;
            }

            @Override
            public ColorRGBA getBeaconColor() {
                return ColorRGBA.Green;
            }

            @Override
            public Control cloneForSpatial(Spatial spatial) {
                return null;
            }

            @Override
            public void setSpatial(Spatial spatial) {

            }

            @Override
            public void update(float tpf) {

            }

            @Override
            public void render(RenderManager rm, ViewPort vp) {

            }

            @Override
            public void write(JmeExporter ex) throws IOException {

            }

            @Override
            public void read(JmeImporter im) throws IOException {

            }
        });
        bulletAppState.getPhysicsSpace().add(node);
    }
}
