package simkuenzi.alonespace.component;

public class Positioning {

    private float xResolution;
    private float yResolution;
    private float border;
    private float rowHeight;

    public Positioning(float xResolution, float yResolution, float border, float rowHeight) {
        this.xResolution = xResolution;
        this.yResolution = yResolution;
        this.border = border;
        this.rowHeight = rowHeight;
    }

    public float getLeftX() {
        return getLeftX(0);
    }

    public float getLeftX(float x) {
        return x + border;
    }

    public float getRightX(float width) {
        return getRightX(0, width);
    }

    public float getRightX(float x, float width) {
        return xResolution - x - width - border;
    }

    public float getBottomY(int row) {
        return rowHeight * row + border;
    }

    public float getTopY(float height) {
        return getTopY(0, height);
    }

    public float getTopY(int row, float height) {
        return yResolution - row * rowHeight - height - border;
    }

    public float getCenterX(float width) {
        return xResolution / 2 - width / 2;
    }

    public float getCenterY(float height) {
        return yResolution / 2 - height / 2;
    }
}
