package simkuenzi.alonespace;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.audio.Environment;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.KeyInput;
import com.jme3.input.RawInputListener;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.event.*;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.CameraControl;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import simkuenzi.alonespace.component.*;

public class Game extends SimpleApplication {

    private static final int RES_X = 1366;
    private static final int RES_Y = 768;
    private static final boolean FULLSCREEN = true;

//    private static final int RES_X = 800;
//    private static final int RES_Y = 600;
//    private static final boolean FULLSCREEN = false;

    private Node shotsNode = new Node();
    private Node shipsNode = new Node();
    private Node explosionsNode = new Node();
    private CameraNode camNode;
    private BulletAppState bulletAppState;

    private Node radarNode = new Node("Radar");


    public static void main(String[] args) {
        Game app = new Game();
        app.setDisplayFps(true);
        app.setDisplayStatView(false);
        app.setShowSettings(false);
        AppSettings settings = new AppSettings(true);
        settings.setResolution(RES_X, RES_Y);
        settings.setSamples(6);
        settings.setFullscreen(FULLSCREEN);
        settings.setUseJoysticks(true);
        app.setSettings(settings);

        app.start();
    }

    @Override
    public void simpleInitApp() {
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        bulletAppState.getPhysicsSpace().setGravity(Vector3f.ZERO);

        Texture westTex = assetManager.loadTexture("simkuenzi/alonespace/sky/space_ft.png");
        Texture eastTex = assetManager.loadTexture("simkuenzi/alonespace/sky/space_bk.png");
        Texture northTex = assetManager.loadTexture("simkuenzi/alonespace/sky/space_lf.png");
        Texture southTex = assetManager.loadTexture("simkuenzi/alonespace/sky/space_rt.png");
        Texture upTex = assetManager.loadTexture("simkuenzi/alonespace/sky/space_up.png");
        Texture downTex = assetManager.loadTexture("simkuenzi/alonespace/sky/space_dn.png");

        final Vector3f normalScale = new Vector3f(1, 1, 1);
        Spatial skySpatial = SkyFactory.createSky(
                assetManager,
                westTex,
                eastTex,
                northTex,
                southTex,
                upTex,
                downTex,
                normalScale);
        rootNode.attachChild(skySpatial);

        rootNode.attachChild(shotsNode);
        rootNode.attachChild(shipsNode);
        rootNode.attachChild(explosionsNode);

        cam.setFrustumFar(10000);

        camNode = new CameraNode("CamNode", cam);
        camNode.setControlDir(CameraControl.ControlDirection.SpatialToCamera);

        Node playerNode = new Node();
        shipsNode.attachChild(playerNode);
        PlayerComponent player = new PlayerComponent(assetManager, inputManager, bulletAppState, camNode, explosionsNode, shotsNode);
        player.apply(playerNode);

        Node asteroidsNode = new Node();
        rootNode.attachChild(asteroidsNode);
        asteroidsNode.setLocalTranslation(0, 0, 2000);
        AsteroidBeltComponent asteroidBelt = new AsteroidBeltComponent(assetManager, bulletAppState, radarNode);
        asteroidBelt.apply(asteroidsNode);

        Node mothershipNode = new Node();
        shipsNode.attachChild(mothershipNode);
        mothershipNode.setLocalTranslation(100, 80, 300);
        MothershipComponent mothership = new MothershipComponent(assetManager, bulletAppState);
        mothership.apply(mothershipNode);

        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        guiNode.detachAllChildren();

        Positioning positioning = new Positioning(RES_X, RES_Y, 10, 25);

        HudComponent hudComponent = new HudComponent(assetManager, positioning, playerNode, shipsNode);
        hudComponent.apply(guiNode);

        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-1, -1, 0));
        DirectionalLight sun2 = new DirectionalLight();
        sun2.setDirection(new Vector3f(1, 1, 0));
        AmbientLight ambientLight = new AmbientLight();
        ambientLight.setColor(new ColorRGBA(.3f, .3f, .3f, 0));
        rootNode.addLight(sun);
        rootNode.addLight(sun2);
        rootNode.addLight(ambientLight);

        AudioNode music = new AudioNode(assetManager, "simkuenzi/alonespace/sound/airship song remix.ogg");
        music.setDirectional(false);
        music.setPositional(false);
        music.setLooping(true);
        rootNode.attachChild(music);
        music.play();

        audioRenderer.setEnvironment(new Environment(Environment.Garage));

        initKeys();
    }

    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        listener.setLocation(camNode.getWorldTranslation());
        listener.setRotation(camNode.getWorldRotation());
    }

    private void initKeys() {
        inputManager.addMapping("flipview", new KeyTrigger(KeyInput.KEY_RETURN));
        inputManager.addListener(actionListener, "flipview");
        inputManager.addRawInputListener(rawInputListener);
    }

    private ActionListener actionListener = (name, isPressed, tpf) -> {
        if (isPressed) {
            camNode.rotate(0, FastMath.PI, 0);
        }
    };

    private void newEnemy(Vector3f location) {
        Node node = new Node("Enemy");
        Quaternion quaternion = new Quaternion();
        quaternion.fromAngles(0, FastMath.PI, 0);
        EnemyComponent enemy = new EnemyComponent(assetManager, bulletAppState, explosionsNode, shotsNode, shipsNode, location, quaternion);
        enemy.apply(node);
        shipsNode.attachChild(node);
    }

    private RawInputListener rawInputListener = new RawInputListener() {
        @Override
        public void beginInput() {

        }

        @Override
        public void endInput() {

        }

        @Override
        public void onJoyAxisEvent(JoyAxisEvent evt) {
        }

        @Override
        public void onJoyButtonEvent(JoyButtonEvent evt) {
            if (evt.getButtonIndex() == 1) {
                camNode.rotate(0, FastMath.PI, 0);
            }
        }

        @Override
        public void onMouseMotionEvent(MouseMotionEvent evt) {

        }

        @Override
        public void onMouseButtonEvent(MouseButtonEvent evt) {

        }

        @Override
        public void onKeyEvent(KeyInputEvent evt) {

        }

        @Override
        public void onTouchEvent(TouchEvent evt) {

        }
    };
}
