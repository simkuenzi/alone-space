package simkuenzi.alonespace.radar;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;

public class RadarScreen extends Node {
    public RadarScreen(AssetManager assetManager) {
        super("RadarScreen");
        Material material = assetManager.loadMaterial("Common/Materials/WhiteColor.j3m");
        drawLine(0, 0, 200, 0, material);
        drawLine(200, 0, 0, 200, material);
        drawLine(200, 200, -200, 0, material);
        drawLine(0, 200, 0, -200, material);
    }

    private void drawLine(int x, int y, int w, int h, Material material) {
        Line line = new Line(new Vector3f(x, y, 0), new Vector3f(x + w, y + h, 0));
        Geometry geometry = new Geometry("RadarScreen Border", line);
        geometry.setMaterial(material);
        attachChild(geometry);
    }
}
