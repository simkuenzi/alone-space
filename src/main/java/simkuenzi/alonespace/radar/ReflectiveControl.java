package simkuenzi.alonespace.radar;

import com.jme3.math.ColorRGBA;
import com.jme3.scene.control.Control;

public interface ReflectiveControl extends Control {
    float getReflection();
    ColorRGBA getBeaconColor();
}
