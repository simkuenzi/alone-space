package simkuenzi.alonespace.radar;

public enum RadarOrientation {
    FRONT, REAR
}
