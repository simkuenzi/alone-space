package simkuenzi.alonespace.radar;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Quad;

public class RadarControl extends AbstractControl {
    private Node beaconsNode;
    private Spatial observer;
    private Node scannedNode;
    private RadarOrientation radarOrientation;
    private Material beaconMaterial;

    public RadarControl(Spatial observer, Node scannedNode, RadarOrientation radarOrientation, AssetManager assetManager) {
        this(observer, scannedNode, radarOrientation, assetManager.loadMaterial("Common/Materials/WhiteColor.j3m"));
    }

    public RadarControl(Spatial observer, Node scannedNode, RadarOrientation radarOrientation, Material beaconMaterial) {
        this.observer = observer;
        this.scannedNode = scannedNode;
        this.radarOrientation = radarOrientation;
        this.beaconMaterial = beaconMaterial;
    }

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        beaconsNode = new Node("Beacons");
        ((Node) spatial).attachChild(beaconsNode);
    }

    @Override
    protected void controlUpdate(float tpf) {
        beaconsNode.getChildren().clear();
        for (Spatial detected : scannedNode.getChildren()) {
            if (detected != observer && detected.getControl(ReflectiveControl.class) != null) {
                ReflectiveControl reflective = detected.getControl(ReflectiveControl.class);
                Vector3f directionToTarget = detected.getWorldTranslation().subtract(observer.getWorldTranslation()).normalize();
                float zAngle = directionToTarget.dot(observer.getWorldRotation().mult(Vector3f.UNIT_Z));
                boolean front = radarOrientation == RadarOrientation.FRONT;
                if (zAngle > 0 && front || zAngle < 0 && !front) {
                    Geometry beacon = new Geometry("", new Quad(5, 5));
                    float xAngle = directionToTarget.dot(observer.getWorldRotation().mult(Vector3f.UNIT_X));
                    float yAngle = directionToTarget.dot(observer.getWorldRotation().mult(Vector3f.UNIT_Y));
                    float distance = observer.getWorldTranslation().distance(detected.getWorldTranslation());
                    float color = 300 / distance * reflective.getReflection();
                    Material material = beaconMaterial.clone();
                    material.setColor("Color", reflective.getBeaconColor().mult(color));
                    beacon.setMaterial(material);
                    beaconsNode.attachChild(beacon);
                    BoundingBox boundingBox = (BoundingBox) getSpatial().getWorldBound();
                    float scaledX = xAngle * -1 * (boundingBox.getXExtent() - 10);
                    float scaledY = yAngle * (boundingBox.getYExtent() - 10);
                    beacon.setLocalTranslation(scaledX + boundingBox.getXExtent(), scaledY + boundingBox.getYExtent(), 0);
                }
            }
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }
}
