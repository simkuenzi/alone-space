package simkuenzi.alonespace.vessel;

import java.util.Arrays;

public class EnergyDistribution {
    private float maxEnergy;
    private OnOffSubsystem stealth = new OnOffSubsystem(this, 4);
    private Subsystem gunCooling = new Subsystem(this, 3);
    private Subsystem throttle = new Subsystem(this, 2);
    private Subsystem engine = new Subsystem(this, 1, 1);

    public EnergyDistribution(float maxEnergy) {
        this.maxEnergy = maxEnergy;
    }

    float demand(Subsystem subsystem) {
        float energyLeft = maxEnergy;
        for (Subsystem other : getSubsystems()) {
            if (other.getPriority() > subsystem.getPriority()) {
                energyLeft -= other.getDemandedEnergy();
            }
        }

        energyLeft = Math.max(0, energyLeft);
        return Math.min(energyLeft, subsystem.getDemandedEnergy());
    }

    private Iterable<Subsystem> getSubsystems() {
        return Arrays.asList(stealth, gunCooling, throttle, engine);
    }

    public OnOffSubsystem getStealth() {
        return stealth;
    }

    public Subsystem getGunCooling() {
        return gunCooling;
    }

    public Subsystem getThrottle() {
        return throttle;
    }

    public Subsystem getEngine() {
        return engine;
    }
}
