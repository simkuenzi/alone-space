package simkuenzi.alonespace.vessel;

import com.jme3.audio.AudioNode;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import simkuenzi.alonespace.component.Component;


public class GunControl extends AbstractControl {
    private Node shotsNode;
    private Component shot;
    private AudioNode audioNode;
    private boolean active;
    private float heat;
    private float coolDown;

    public GunControl(Node shotsNode, Component shot, AudioNode audioNode) {
        this.shotsNode = shotsNode;
        this.shot = shot;
        this.audioNode = audioNode;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setCoolDown(float coolDown) {
        this.coolDown = coolDown * 4f;
    }

    private void fire() {
        Node node = new Node();
        node.setLocalTranslation(spatial.getWorldTranslation());
        node.setLocalRotation(spatial.getWorldRotation());
        shot.apply(node);
        shotsNode.attachChild(node);
        audioNode.playInstance();
    }

    @Override
    protected void controlUpdate(float tpf) {
        if (heat <= 0 && active) {
            fire();
            heat = 1;
        } else {
            heat -= coolDown * tpf;
            heat = Math.max(0, heat);
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {

    }

    public float getHeat() {
        return heat;
    }
}
