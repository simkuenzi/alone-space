package simkuenzi.alonespace.vessel;

import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class ExplosionControl extends AbstractControl {
    private float time = 0;
    private int state = 0;

    private AudioNode audioNode;
    private ParticleEmitter flame;
    private ParticleEmitter flash;
    private ParticleEmitter spark;
    private ParticleEmitter roundspark;
    private ParticleEmitter smoketrail;
    private ParticleEmitter debris;
    private ParticleEmitter shockwave;

    public ExplosionControl(AudioNode audioNode, ParticleEmitter flame, ParticleEmitter flash, ParticleEmitter spark, ParticleEmitter roundspark, ParticleEmitter smoketrail, ParticleEmitter debris, ParticleEmitter shockwave) {
        this.audioNode = audioNode;
        this.flame = flame;
        this.flash = flash;
        this.spark = spark;
        this.roundspark = roundspark;
        this.smoketrail = smoketrail;
        this.debris = debris;
        this.shockwave = shockwave;
    }

    @Override
    protected void controlUpdate(float tpf) {
        float speed = 0.001f;
        time += tpf / speed;
        if (time > 1f && state == 0) {
            flash.emitAllParticles();
            spark.emitAllParticles();
            smoketrail.emitAllParticles();
            debris.emitAllParticles();
            shockwave.emitAllParticles();
            audioNode.play();
            state++;
        }
        if (time > 1f + .05f / speed && state == 1) {
            flame.emitAllParticles();
            roundspark.emitAllParticles();
            state++;
        }

        if (time > 5 / speed && state == 2) {
            spatial.removeFromParent();
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {

    }
}
