package simkuenzi.alonespace.vessel;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import simkuenzi.alonespace.component.Component;
import simkuenzi.alonespace.radar.ReflectiveControl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class VesselControl extends RigidBodyControl implements PhysicsTickListener, ReflectiveControl {
    private Component explosion;
    private Node explosionsNode;
    private float rotation;
    private float diveClimb;
    private float leftRight;
    private boolean fire;
    private int condition = 20;
    private int activeGun = 0;
    private int gunLinking;
    private EnergyDistribution energyDistribution = new EnergyDistribution(1.5f);
    private List<Hit> hits = new ArrayList<>();

    public VesselControl(Component explosion, Node explosionsNode) {
        this.explosion = explosion;
        this.explosionsNode = explosionsNode;
        energyDistribution.getGunCooling().setDemandedEnergy(.5f);
    }

    public EnergyDistribution getEnergyDistribution() {
        return energyDistribution;
    }

    public void changeThrottle(float throttle) {
        energyDistribution.getThrottle().setDemandedEnergy(throttle);
    }

    public void changeRotation(float rotation) {
        this.rotation = rotation;
    }

    public void changeClimbDive(float diveClimb) {
        this.diveClimb = diveClimb;
    }

    public void changeLeftRight(float leftRight) {
        this.leftRight = leftRight;
    }

    public void changeFire(boolean fire) {
        this.fire = fire;
    }

    public void changeGunLink() {
        gunLinking = gunLinking < 2 ? gunLinking + 1 : 0;
        activeGun = 0;
    }

    @Override
    public float getReflection() {
        return energyDistribution.getStealth().isEnabled() ? 0 : 1;
    }

    @Override
    public ColorRGBA getBeaconColor() {
        return ColorRGBA.Red;
    }

    public void hit(int damage, Vector3f direction, Vector3f contactPoint) {
        Hit hit = new Hit();
        hit.damage = damage;
        hit.direction = direction;
        hit.contactPoint = contactPoint;
        hits.add(hit);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        if (condition <= 0) {
            Node explosionNode = new Node();
            explosionNode.setLocalTranslation(spatial.getWorldTranslation());
            explosion.apply(explosionNode);
            explosionsNode.attachChild(explosionNode);
            spatial.removeFromParent();
            space.remove(spatial);
        }

        for (GunControl gun : getGuns()) {
            gun.setCoolDown(energyDistribution.getGunCooling().getActualEnergy());
        }

        boolean noGunActive = true;
        int gunLinkPow = (int) FastMath.pow(2, gunLinking);
        for (int i = 0; i < getGuns().size(); i+=gunLinkPow) {
            GunControl previous = i == 0 ? getGuns().get(getGuns().size() - 1) : getGuns().get(i - 1);
            float ratio = (float) (getGuns().size()-1) / ((float) getGuns().size() * (float) gunLinkPow);

            boolean fire = this.fire && (getGuns().get(i).getHeat() <= 0 && previous.getHeat() <= ratio && noGunActive && activeGun == i);
            if (fire) {
                noGunActive = false;
                activeGun = activeGun == getGuns().size() - gunLinkPow ? 0 : activeGun + gunLinkPow;
            }

            for (int j = 0; j < gunLinkPow; j++) {
                getGuns().get(i + j).setActive(fire);
            }
        }
    }

    @Override
    public void prePhysicsTick(PhysicsSpace space, float tpf) {
        applyCentralForce(spatial.getWorldRotation().mult(new Vector3f(0, 0, energyDistribution.getThrottle().getActualEnergy() * 100)));
        applyTorque(spatial.getWorldRotation().mult(new Vector3f(0, leftRight * -30, 0)));
        applyTorque(spatial.getWorldRotation().mult(new Vector3f(diveClimb * -30, 0, 0)));
        applyTorque(spatial.getWorldRotation().mult(new Vector3f(0, 0, rotation * 30)));

        for (Hit hit : hits) {
            condition -= hit.damage;
            applyImpulse(hit.direction.mult(6), hit.contactPoint);
        }

        hits.clear();
    }

    public List<GunControl> getGuns() {
        if (spatial instanceof Node) {
            return ((Node) spatial).getChildren().stream()
                    .filter(x -> x.getControl(GunControl.class) != null)
                    .map(x -> x.getControl(GunControl.class))
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public void physicsTick(PhysicsSpace space, float tpf) {

    }

    public int getCondition() {
        return condition;
    }

    public Spatial getSpatial() {
        return spatial;
    }

    private static class Hit {
        int damage;
        Vector3f direction;
        public Vector3f contactPoint;
    }
}
