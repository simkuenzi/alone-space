package simkuenzi.alonespace.vessel;

import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.GhostControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class ShotControl extends GhostControl implements PhysicsCollisionListener {
    private boolean impact;
    private float distance = 500;

    public ShotControl(CollisionShape collisionShape) {
        super(collisionShape);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        spatial.move(spatial.getWorldRotation().mult(new Vector3f(0, 0, 1000 * tpf)));
        distance -= 1000 * tpf;
        if (impact || distance <= 0) {
            spatial.removeFromParent();
            space.remove(spatial);
        }
    }

    @Override
    public void collision(PhysicsCollisionEvent event) {
        if (event.getNodeA() == spatial || event.getNodeB() == spatial) {
            Spatial target = event.getNodeA() == spatial ? event.getNodeB() : event.getNodeA();
            Vector3f contactPoint = event.getNodeA() == spatial ? event.getLocalPointB() : event.getLocalPointA();
            if (target.getControl(VesselControl.class) != null) {
                target.getControl(VesselControl.class).hit(1, spatial.getWorldRotation().mult(Vector3f.UNIT_Z), contactPoint);
            }
            impact = true;
        }
    }
}
