package simkuenzi.alonespace.vessel;

public class OnOffSubsystem extends Subsystem {
    public OnOffSubsystem(EnergyDistribution energyDistribution, int priority) {
        super(energyDistribution, priority);
    }

    public boolean isEnabled() {
        return getActualEnergy() == 1;
    }

    public void setEnabled(boolean enabled) {
        setDemandedEnergy(enabled ? 1 : 0);
    }
}
