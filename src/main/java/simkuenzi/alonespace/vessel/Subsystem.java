package simkuenzi.alonespace.vessel;

public class Subsystem {
    private EnergyDistribution energyDistribution;
    private int priority;
    private float demandedEnergy;

    public Subsystem(EnergyDistribution energyDistribution, int priority) {
        this(energyDistribution, priority, 0);
    }

    public Subsystem(EnergyDistribution energyDistribution, int priority, float demandedEnergy) {
        this.energyDistribution = energyDistribution;
        this.priority = priority;
        this.demandedEnergy = demandedEnergy;
    }

    public float getDemandedEnergy() {
        return demandedEnergy;
    }

    public void setDemandedEnergy(float demandedEnergy) {
        this.demandedEnergy = Math.max(Math.min(demandedEnergy, 1), 0);
    }

    public float getActualEnergy() {
        return energyDistribution.demand(this);
    }

    public int getPriority() {
        return priority;
    }
}
