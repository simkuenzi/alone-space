package simkuenzi.alonespace.pilot;

import com.jme3.scene.Spatial;
import simkuenzi.alonespace.vessel.VesselControl;

public class HaltManeuver implements Maneuver {

    @Override
    public void execute(VesselControl vessel, Spatial target) {
        vessel.changeLeftRight(0);
        vessel.changeClimbDive(0);
        vessel.changeRotation(0);
        vessel.changeThrottle(0);
        vessel.changeFire(false);
    }

    @Override
    public boolean applies(VesselControl vessel, Spatial target) {
        return true;
    }
}
