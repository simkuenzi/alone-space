package simkuenzi.alonespace.pilot;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import simkuenzi.alonespace.vessel.VesselControl;

public class ApproachManeuver implements Maneuver {
    @Override
    public void execute(VesselControl vessel, Spatial target) {
        Vector3f directionToTarget = target.getWorldTranslation().subtract(vessel.getSpatial().getWorldTranslation()).normalize();
        float x = directionToTarget.dot(vessel.getSpatial().getWorldRotation().mult(Vector3f.UNIT_X));
        float y = directionToTarget.dot(vessel.getSpatial().getWorldRotation().mult(Vector3f.UNIT_Y));
        vessel.changeFire(false);
        vessel.changeLeftRight(-x);
        vessel.changeClimbDive(y);
        vessel.changeThrottle(1);
    }

    @Override
    public boolean applies(VesselControl vessel, Spatial target) {
        return target != null && vessel.getSpatial().getWorldTranslation().distance(target.getWorldTranslation()) >= 40;
    }
}
