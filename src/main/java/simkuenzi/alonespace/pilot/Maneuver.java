package simkuenzi.alonespace.pilot;

import com.jme3.scene.Spatial;
import simkuenzi.alonespace.vessel.VesselControl;

public interface Maneuver {
    void execute(VesselControl vessel, Spatial target);
    boolean applies(VesselControl vessel, Spatial target);
}
