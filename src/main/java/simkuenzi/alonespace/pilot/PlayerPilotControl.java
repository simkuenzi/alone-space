package simkuenzi.alonespace.pilot;

import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.RawInputListener;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.event.*;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;
import simkuenzi.alonespace.vessel.VesselControl;

public class PlayerPilotControl extends AbstractControl {

    private boolean gunCoolingPlus;
    private boolean gunCoolingMinus;

    public PlayerPilotControl(InputManager inputManager) {
        inputManager.addMapping("dive", new KeyTrigger(KeyInput.KEY_UP));
        inputManager.addMapping("climb", new KeyTrigger(KeyInput.KEY_DOWN));
        inputManager.addMapping("left", new KeyTrigger(KeyInput.KEY_LEFT));
        inputManager.addMapping("right", new KeyTrigger(KeyInput.KEY_RIGHT));
        inputManager.addMapping("rot-left", new KeyTrigger(KeyInput.KEY_Y));
        inputManager.addMapping("rot-right", new KeyTrigger(KeyInput.KEY_X));
        inputManager.addMapping("fire", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("gun-cooling-plus", new KeyTrigger(KeyInput.KEY_ADD));
        inputManager.addMapping("gun-cooling-minus", new KeyTrigger(KeyInput.KEY_SUBTRACT));
        inputManager.addMapping("stealth", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("gun-link", new KeyTrigger(KeyInput.KEY_G));
        inputManager.addMapping("throttle-1", new KeyTrigger(KeyInput.KEY_1));
        inputManager.addMapping("throttle-2", new KeyTrigger(KeyInput.KEY_2));
        inputManager.addMapping("throttle-3", new KeyTrigger(KeyInput.KEY_3));
        inputManager.addMapping("throttle-4", new KeyTrigger(KeyInput.KEY_4));
        inputManager.addMapping("throttle-5", new KeyTrigger(KeyInput.KEY_5));
        inputManager.addMapping("throttle-6", new KeyTrigger(KeyInput.KEY_6));
        inputManager.addMapping("throttle-7", new KeyTrigger(KeyInput.KEY_7));
        inputManager.addMapping("throttle-8", new KeyTrigger(KeyInput.KEY_8));
        inputManager.addMapping("throttle-9", new KeyTrigger(KeyInput.KEY_9));
        inputManager.addMapping("throttle-full", new KeyTrigger(KeyInput.KEY_0));
        inputManager.addMapping("throttle-off", new KeyTrigger(KeyInput.KEY_BACK));
        inputManager.addListener(actionListener,
                "dive",
                "climb",
                "left",
                "right",
                "rot-left",
                "rot-right",
                "fire",
                "gun-cooling-plus",
                "gun-cooling-minus",
                "stealth",
                "gun-link",
                "throttle-1",
                "throttle-2",
                "throttle-3",
                "throttle-4",
                "throttle-5",
                "throttle-6",
                "throttle-7",
                "throttle-8",
                "throttle-9",
                "throttle-full",
                "throttle-off");
        inputManager.addRawInputListener(rawInputListener);
    }

    @Override
    protected void controlUpdate(float tpf) {
        VesselControl vessel = spatial.getControl(VesselControl.class);
        if (vessel != null) {
            if (gunCoolingPlus) {
                vessel.getEnergyDistribution().getGunCooling().setDemandedEnergy(
                        vessel.getEnergyDistribution().getGunCooling().getDemandedEnergy() + 0.3f * tpf);
            }
            if (gunCoolingMinus) {
                vessel.getEnergyDistribution().getGunCooling().setDemandedEnergy(
                        vessel.getEnergyDistribution().getGunCooling().getDemandedEnergy() - 0.3f * tpf);
            }
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {

    }

    private ActionListener actionListener = (name, isPressed, tpf) -> {
        VesselControl vessel = spatial.getControl(VesselControl.class);
        if (vessel != null) {
            switch (name) {
                case "left":
                    vessel.changeLeftRight(isPressed ? -1 : 0);
                    break;
                case "right":
                    vessel.changeLeftRight(isPressed ? 1 : 0);
                    break;
                case "dive":
                    vessel.changeClimbDive(isPressed ? -1 : 0);
                    break;
                case "climb":
                    vessel.changeClimbDive(isPressed ? 1 : 0);
                    break;
                case "rot-left":
                    vessel.changeRotation(isPressed ? -1 : 0);
                    break;
                case "rot-right":
                    vessel.changeRotation(isPressed ? 1 : 0);
                    break;
                case "fire":
                    vessel.changeFire(isPressed);
                    break;
                case "gun-cooling-plus":
                    gunCoolingPlus = isPressed;
                    break;
                case "gun-cooling-minus":
                    gunCoolingMinus = isPressed;
                    break;
                case "stealth":
                    if (!isPressed) {
                        vessel.getEnergyDistribution().getStealth().setEnabled(!vessel.getEnergyDistribution().getStealth().isEnabled());
                    }
                    break;
                case "gun-link":
                    if (!isPressed) {
                        vessel.changeGunLink();
                    }
                    break;
                case "throttle-1":
                    vessel.changeThrottle(.1f);
                    break;
                case "throttle-2":
                    vessel.changeThrottle(.2f);
                    break;
                case "throttle-3":
                    vessel.changeThrottle(.3f);
                    break;
                case "throttle-4":
                    vessel.changeThrottle(.4f);
                    break;
                case "throttle-5":
                    vessel.changeThrottle(.5f);
                    break;
                case "throttle-6":
                    vessel.changeThrottle(.6f);
                    break;
                case "throttle-7":
                    vessel.changeThrottle(.7f);
                    break;
                case "throttle-8":
                    vessel.changeThrottle(.8f);
                    break;
                case "throttle-9":
                    vessel.changeThrottle(.9f);
                    break;
                case "throttle-full":
                    vessel.changeThrottle(1f);
                    break;
                case "throttle-off":
                    vessel.changeThrottle(0);
                    break;
            }
        }
    };

    private RawInputListener rawInputListener = new RawInputListener() {
        @Override
        public void beginInput() {

        }

        @Override
        public void endInput() {

        }

        @Override
        public void onJoyAxisEvent(JoyAxisEvent evt) {
            VesselControl vessel = spatial.getControl(VesselControl.class);
            if (vessel != null) {
                if (evt.getAxisIndex() == 0) {
                    vessel.changeLeftRight(Math.round(evt.getValue() * 10f) / 10f);
                } else if (evt.getAxisIndex() == 1) {
                    vessel.changeClimbDive(Math.round(evt.getValue() * 10f) / 10f);
                } else if (evt.getAxisIndex() == 2) {
                    vessel.changeThrottle(Math.round((evt.getValue() * -1f + 1f) / 2f * 10f) / 10f);
                } else if (evt.getAxisIndex() == 3) {
                    vessel.changeRotation(Math.round(evt.getValue() * 10f) / 10f);
                }
            }
        }

        @Override
        public void onJoyButtonEvent(JoyButtonEvent evt) {
            VesselControl vessel = spatial.getControl(VesselControl.class);
            if (vessel != null) {
                if (evt.getButtonIndex() == 0) {
                    vessel.changeFire(evt.isPressed());
                }
            }
        }

        @Override
        public void onMouseMotionEvent(MouseMotionEvent evt) {

        }

        @Override
        public void onMouseButtonEvent(MouseButtonEvent evt) {

        }

        @Override
        public void onKeyEvent(KeyInputEvent evt) {

        }

        @Override
        public void onTouchEvent(TouchEvent evt) {

        }
    };
}
