package simkuenzi.alonespace.pilot;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import simkuenzi.alonespace.vessel.VesselControl;

public class AttackManeuver implements Maneuver {
    @Override
    public void execute(VesselControl vessel, Spatial target) {
        vessel.changeFire(true);
    }

    @Override
    public boolean applies(VesselControl vessel, Spatial target) {
        if (target != null) {
            float distance = vessel.getSpatial().getWorldTranslation().distance(target.getWorldTranslation());
            Vector3f directionToTarget = target.getWorldTranslation().subtract(vessel.getSpatial().getWorldTranslation()).normalize();
            float xAngle = directionToTarget.dot(vessel.getSpatial().getWorldRotation().mult(Vector3f.UNIT_X));
            float yAngle = directionToTarget.dot(vessel.getSpatial().getWorldRotation().mult(Vector3f.UNIT_Y));
            return distance <= 150 && xAngle < .3 && xAngle > -.3 && yAngle < .3 && yAngle > -.3;
        } else {
            return false;
        }
    }
}
