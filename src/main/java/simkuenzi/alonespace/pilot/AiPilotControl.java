package simkuenzi.alonespace.pilot;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import simkuenzi.alonespace.vessel.VesselControl;

import java.util.ArrayList;
import java.util.List;

public class AiPilotControl extends AbstractControl {
    private Node targetsNode;
    private Spatial target;
    private List<Maneuver> maneuvers = new ArrayList<>();
    private Maneuver defaultManeuver = new ApproachManeuver();

    public AiPilotControl(Node targetsNode) {
        this.targetsNode = targetsNode;
        maneuvers.add(new AttackManeuver());
        maneuvers.add(new ApproachManeuver());
    }

    @Override
    protected void controlUpdate(float tpf) {
        VesselControl vessel = spatial.getControl(VesselControl.class);
        if (vessel != null) {
            selectTarget(vessel);
            Maneuver maneuver = selectManeuver(vessel);
            maneuver.execute(vessel, target);
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {

    }

    private void selectTarget(VesselControl vessel) {
        if (target == null || target.getParent() == null) {
            float minDistance = Float.MAX_VALUE;
            for (Spatial target : targetsNode.getChildren()) {
                if (target.getControl(VesselControl.class) != vessel) {
                    float distance = target.getWorldTranslation().distance(vessel.getSpatial().getWorldTranslation());
                    if (distance < minDistance) {
                        minDistance = distance;
                        this.target = target;
                    }
                }
            }
        }
    }

    private Maneuver selectManeuver(VesselControl vessel) {
        Maneuver selectedManeuver = null;
        for (Maneuver maneuver : maneuvers) {
            if (maneuver.applies(vessel, target)) {
                selectedManeuver = maneuver;
                break;
            }
        }
        return selectedManeuver != null ? selectedManeuver : defaultManeuver;
    }
}
