package simkuenzi.alonespace.display;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;

public class Display extends Node {
    public Display(String label, AssetManager assetManager) {
        this(label, assetManager, 200);
    }

    public Display(String label, AssetManager assetManager, float widht) {
        Quad quad = new Quad(widht, 20);
        Material material = assetManager.loadMaterial("Common/Materials/RedColor.j3m");
        Geometry geometry = new Geometry("", quad);
        geometry.setMaterial(material);
        attachChild(geometry);

        BitmapFont font = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText text = new BitmapText(font, false);
        text.setSize(14);
        text.setText(label);
        text.setColor(ColorRGBA.Black);
        text.setLocalTranslation(widht/2 - text.getLineWidth()/2, 19, 1);
        attachChild(text);
    }
}
