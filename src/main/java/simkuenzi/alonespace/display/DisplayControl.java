package simkuenzi.alonespace.display;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Quad;
import simkuenzi.alonespace.function.FloatSupplier;

public class DisplayControl extends AbstractControl {
    private FloatSupplier value;
    private FloatSupplier maxValue;
    private FloatSupplier demandedValue;
    private Node valueNode;
    private Material valueMaterial;
    private Material demandedValueMaterial;
    private boolean mirror;

    public DisplayControl(AssetManager assetManager, FloatSupplier value, FloatSupplier maxValue ,FloatSupplier demandedValue) {
        this.demandedValue = demandedValue;
        this.maxValue = maxValue;
        this.value = value;
        valueMaterial = assetManager.loadMaterial("Common/Materials/WhiteColor.j3m");
        valueMaterial.setColor("Color", ColorRGBA.Green);
        demandedValueMaterial = assetManager.loadMaterial("Common/Materials/WhiteColor.j3m");
        demandedValueMaterial.setColor("Color", ColorRGBA.Yellow);
    }

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        valueNode = new Node();
        ((Node) spatial).attachChild(valueNode);
    }

    public boolean isMirror() {
        return mirror;
    }

    public void setMirror(boolean mirror) {
        this.mirror = mirror;
    }

    @Override
    protected void controlUpdate(float tpf) {
        valueNode.getChildren().clear();
        BoundingBox boundingBox = (BoundingBox) spatial.getWorldBound();
        float rel = value.getAsFloat() / maxValue.getAsFloat();
        float width = 2 * boundingBox.getXExtent() * rel;
        Quad quad = new Quad(width, 2 * boundingBox.getYExtent());
        Geometry geometry = new Geometry("Value Display", quad);
        geometry.setMaterial(valueMaterial);
        if (mirror) {
            geometry.setLocalTranslation(2 * boundingBox.getXExtent() - width, 0, 0);
        }
        valueNode.attachChild(geometry);
        if (demandedValue != null) {
            float demanedRel = demandedValue.getAsFloat() / maxValue.getAsFloat();
            float demanedX = 2 * boundingBox.getXExtent() * demanedRel;
            if (mirror) {
                demanedX = 2 * boundingBox.getXExtent() - demanedX;
            }
            Line line = new Line(new Vector3f(demanedX, 0, 0), new Vector3f(demanedX, 2 * boundingBox.getYExtent(), 0));
            Geometry demandedGeometry = new Geometry("Demanded Value Display", line);
            demandedGeometry.setMaterial(demandedValueMaterial);
            valueNode.attachChild(demandedGeometry);
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {

    }
}
